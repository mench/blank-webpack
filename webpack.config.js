'use strict';

const webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || 'development';



module.exports = {

    entry: {
        app: ['./app/app.js']
    },
    output: {
        path: __dirname + '/build/',
        publicPath: '/build/',
        filename: '[name].js'
    },


    devtool: process.env.NODE_ENV === 'development' ? 'eval-source-map' : null,

    module: {
        loaders: [
            {
                test: [/\.js?$/],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.styl$/,
                loaders: ['style-loader', 'css-loader', 'stylus'],
            },


            {
                test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
                loader: 'file-loader?name=[path][name].[ext]?[hash]'
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },


        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        })
    ],

    devServer: {
        port: 5000,
        hot: true,
        progress: true,
        colors: true
    }
};

if ( NODE_ENV === 'production' ) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    )
}