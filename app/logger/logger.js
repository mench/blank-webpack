'use strict';

export default function(message, type) {
    if ( NODE_ENV === 'development' ) {
        console[type || 'log'](message);
    }
}